![](https://elioway.gitlab.io/elioangels/twig/elio-twig-logo.png)

> Not a complete git **the elioWay**

# twig-branciao ![experimental](/artwork/icon/experimental/favicon.png "experimental")

**twig** is a git branch-management command-line toolset tailored to elioWay.

- [twig Documentation](https://elioway.gitlab.io/elioangels/twig)
- [twig Demo](https://elioway.gitlab.io/elioangels/twig/demo.html)

## Installing

- [Installing twig](https://elioway.gitlab.io/elioangels/twig/installing.html)

## Nutshell

### `twig checkout branch`

### `twig commit branch "changes on the newbranch"`

### `twig merge branch merge2`

### `twig push branch`

- [twig Quickstart](https://elioway.gitlab.io/elioangels/twig/quickstart.html)
- [twig Credits](https://elioway.gitlab.io/elioangels/twig/credits.html)

![](https://elioway.gitlab.io/elioangels/twig/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
