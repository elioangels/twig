# twig commands

Commands run against all repos listed in the BRANCIAOS file (.brancios or branches.txt).

Most commands only work on repos mathcing the `<branch>` parameter.

## `twig`

`twig`

Display **twig**'s commands.

## `branches`

`twig branches <BRANCH>`

## `checkout`

`twig checkout <BRANCH>`

## `commit`

`twig commit <BRANCH> [<MSG>]`

## `merge`

`twig merge <BRANCH> [<MERGE2BRANCH>]`

## `pull`

`twig pull <BRANCH>`

## `push`

`twig push <BRANCH>`

## `snap`

`twig snap <BRANCH>`

Delete the local `BRANCH` on any **BRANCIAOS**.

## `status`

`twig status`
