# twigs Angels

Thank you, contributors. You are Angels. We welcome any help. A minor fix or feature added, it doesn't matter. Any help welcomed. Correct our grammar. Make our code more succinct. Rewrite it under the same rules in an entirely different language. We want to see what you would do, and where we can improve.

Please commit with us to doing things the elioWay and using **twig** as your git management command line tool.

## The Laws of Twig

1. The app matters. The path name where you run a **twig** command matters. The repo at that path is known as the commanding app.

2. **twig** will not run unless the commanding app has a file called `.branciaos`. It should contain the list of relative/absolute path names of other repos in your file system. They will be under the commanding app's control.

3. **twig** will not run unless the commanding AND targeted repo have a file called `.twig` AND the branch listed in that file matches the one from the commanding app.

4. **twig** cannot checkout `master`. Not on the commanding app. Not anywhere.

5. All **twig** commands run against `All` the repos listed in `.branciaos` file.

6. Start with `twig innit <branchName>`

## Commands

### `twig innit <branchName>`

From a porcelian branch only, `twig innit <branchName>` will checkout a new branch called `<branchName>` which **twig** accepts as `master`.

As with `All` **twig** commands it will repeat it for all the repos listed in the commanding app's `.branciaos` file. `But` only long as they have a `.twig` file with the `<branchName>` as its contents.

From this point the commanding app is responsible for remembering the original branches of its own and command apps listed in the `.branciaos` file.

You can now use the `twig checkout <newBranchName>` command and it will put any commanded apps on to this branch `But` only if they are porcelain `And` if their `.twig` file matches the commanding app's.

When you use `twig merge`... what?
