# twig Credits

## Artwork

- <https://openclipart.org/detail/217197/a-simple-tree>

## This was useful

- <https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab>
- <https://stackoverflow.com/questions/37895592/how-to-use-multiple-ssh-keys-for-multiple-gitlab-accounts-with-the-same-host>

# Stuff that looks relevant and interesting

- [git reset --soft b7ec061](http://darcs.net/Using#informal-documentation)
- [darcs git alternative](https://superuser.com/questions/751699/is-there-a-way-to-edit-a-commit-message-on-github)
- [simple-git](https://www.npmjs.com/package/simple-git)

## Consider Using

- [gluegun](https://github.com/infinitered/gluegun#readme)
- [oclif](https://oclif.io/docs/introduction)
