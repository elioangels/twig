# twig

<figure>
  <img src="star.png" alt="">
</figure>

> Not a complete git **the elioWay**

![experimental](//docs.elioway/./elioangels/icon/experimental/favicon.png "experimental")

**twig** is a git branch-management command-line toolset tailored to elioWay.

Developers can use **twig** alongside _git_ to keep the app repos on the same branch as the requirements/branch.txt file of the project they are working (known as the _home branch_), or by putting them all on a new temporary _work branch_, which you will merge back to the _home branch_ later.

Like most things the elioWay, **twig** is opinated. It works best with elioWay project set up, but can support whatever way you organise your projects. However certain features, like setting the remote url, are programmed under the assumption the repos are being pushed to the elioWay account.

There are plans to convert it from bash to Python and Nodejs.

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a>
  <a href="commands.html">
  <button>Commands</button>
</a></div>
