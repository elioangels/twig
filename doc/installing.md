# Installing twig

- [Prerequisites](./prerequisites)

## Git install

```shell
git clone https://${GITHOST}/elioangels/twig ~/bin/twig
```

Add `bin` or `twig` to your shell PATH.

```shell
export PATH="~/bin:$PATH"

#fish
set -U fish_user_paths /home/tim/Dev/elioway/elioangels/twig $fish_user_paths
```

## Testing

```shell
cd elioangels
cd twig
```

Make sure **branciao-fire** is installed as a sub repo.

- [branciao-fire](https://${GITHOST}/elioangels/branciao-fire.git)

```shell
git submodule update
```

Run tests:

```shell
# Run all tests
tests/_runner
# Test individual test
tests/test_commit
```
