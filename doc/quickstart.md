# twig Quickstart

- [Prerequisites](./prerequisites)

## Key

- **REQS** = the requirements/branch.txt file.
- **PRARS** = the project repo (i.e. repo of current folder) and all the repos mentioned in **REQS**.

## Nutshell

```shell
cd ~/repo/projects/waterboard_org
```

Keep `projects/<myproject>/requirements/branch.txt` up to date for the branch in your active project.

Type `twig checkout` to checkout **PRARS** onto the _home branch_ mentioned in **REQS**.

Type `twig checkout <branch>` to checkout **PRARS** onto `<branch>`.

Type `twig merge <branch>` to merge **PRARS** from `<branch>` onto their _home branch_ mentioned in **REQS**.

Type `twig merge <branch> <targetbranch>` to merge **PRARS** from `<branch>` to `<targetbranch>`.

## Setup

Start in your project folder.

```shell
cd ~/path/to/dev/project/myproject_com
git checkout master
```

Create/Edit a file called `.branciaos`.

You'll need paths relative to and from all the apps listed so that they can share a common root path (which twig chooses from the current project);

You should also add a row for the current project.

```
../app/api|3802-api_client-fixture
../app/base|main
../app/contact|3699-myproject-demo
../app/gallery|main
../app/gdpr|3699-myproject-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|main
../app/message|main
../app/search|3699-myproject-demo
../project/myproject_com|main
```

Now you can call `twig checkout` and it will put all the apps on the given branches.

### Working on a new parent ticket

```shell
cd ~/path/to/dev/project/myproject_com
git checkout 4444-cool-new-feature
```

Edit `requirements/branch.txt` and change the _home branch_

```
../app/api|3802-api_client-fixture
../app/base|main
../app/contact|3699-myproject-demo
../app/gallery|main
../app/gdpr|3699-myproject-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|main
../app/message|main
../app/search|3699-myproject-demo
../project/myproject_com|4444-cool-new-feature
```

Now any new branches you create on this "cool-new-feature" will be merged to this ticket. Commit this change.

```shell
git add .
git commit -m "starting cool-new-feature"
```

_Optional_: You could also call `twig checkout 4444-cool-new-feature` here and put all the apps on the same starting branch as **myproject_com**.

## Branch out with a new child ticket

Start work on the new-feature. You probably want a new branch for any pieces of smaller work on 4444-cool-new-feature.

```shell
git add .
twig checkout 5555-starting-cool-feature-restapi
```

This will put this project and _all_ its **PRARS** on `5555-cool-feature-api`.

Make changes on this **myproject_com**. Make changes on any **app/api** and **app/mail**.

```shell
twig commit "working in progress" 5555-cool-feature-api
```

This "adds all" and commit on **myproject_com**, **app/api** and **app/mail**

## Merge back to the original branches

When you are happy with your changes.

```shell
twig merge
```

This command first "adds all" and commits on any **PRARS** with changes. Then it merges

- **myproject_com** to `4444-cool-new-feature`
- **app/api** to `3802-api_client-fixture`
- **app/mail** to `master`
- ...etc: + other unedited **PRARS** to _home branch_

_Optional_: You could also call `twig merge 5555-cool-feature-api 4444-cool-new-feature` here and put all the apps back to the same starting branch as **myproject_com**

Finally, delete `5555-starting-cool-feature-restapi` from all **PRARS**

```shell
twig snap 5555-starting-cool-feature-restapi
```

Now you can start the next child ticket. And so on.

## Finishing work parent ticket

Eventually you will have finished working on the parent ticket and are ready to merge your changes back.

Edit `requirements/branch.txt` and change the _home branch_ to `master` or whatever.

```
../app/api|3802-api_client-fixture
../app/base|main
../app/contact|3699-myproject-demo
../app/gallery|main
../app/gdpr|3699-myproject-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|main
../app/message|main
../app/search|3699-myproject-demo
../project/myproject_com|main
```

```shell
git add .
git commit -m "finishing cool-new-feature"
twig merge
```

Given that we have now indicated we want **myproject_com** back on the `master` branch; the `twig merge` command merges:

- **myproject_com** to `master`
- **app/api** to `3802-api_client-fixture`
- **app/mail** to `master`
- ...etc: + other unedited **PRARS** to _home branch_

Finally, delete `4444-cool-new-feature` myproject_com

```shell
twig snap 4444-cool-new-feature
```

However, at the risk of being impertinent, **twig** has other commands.

## Next

- [twig Command List](./commands)
