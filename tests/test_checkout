#!/bin/bash
set +e
TESTNAME=$(basename "${BASH_SOURCE[0]}")
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/${TESTNAME}"
SCRIPTPATH="$( cd "$(dirname "$ABSOLUTE_PATH")" ; pwd -P )"
TWIGPATH=`echo $SCRIPTPATH |xargs dirname`
source "${TWIGPATH}/tests/_settings"
source "${BRANCIAOFIREPATH}/tests/asserts"

explain "${TESTNAME}"

before_each "${TESTFIRE2} without parameter"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_flower" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" flower
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE2}"
  git checkout leaf
  source "${TWIGPATH}/twig" checkout
} # &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" leaf


before_each "${TESTFIRE3} without parameter"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_flower" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" flower
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE3}"
  git checkout flower
  source "${TWIGPATH}/twig" checkout
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" flower
assert_firebrand_is_on_branch "${TESTFIRE3}" flower


before_each "${TESTFIRE1} without parameter"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_flower" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" flower
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE1}"
  git checkout twig
  source "${TWIGPATH}/twig" checkout
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" twig
assert_firebrand_is_on_branch "${TESTFIRE3}" twig


before_each "${TESTFIRE2} fails when branches arent porcelian"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_twig" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" twig
assert_firebrand_is_on_branch "${TESTFIRE3}" twig
{
  # change BRAND2 on it's new-branch
  echo "uncommitted change" > "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-uncommitted.txt"
  # change BRAND3 on it's new-branch
  echo "uncommitted change" > "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-uncommitted.txt"
  # checkout BRAND2
  cd "${BRANCIAOFIREPATH}/${TESTFIRE1}"
  source "${TWIGPATH}/twig" checkout "new-branch"
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE1}" "new-branch"
assert_firebrand_is_porcelian "${TESTFIRE1}"
assert_firebrand_is_on_branch "${TESTFIRE2}" twig
assert_firebrand_is_not_porcelian "${TESTFIRE2}"
assert_firebrand_is_on_branch "${TESTFIRE3}" twig
assert_firebrand_is_not_porcelian "${TESTFIRE3}"
